import { log } from "console";
import { AppDataSource } from "./data-source"
import { User } from "./entity/User"
import { Role } from "./entity/Role";

AppDataSource.initialize().then(async () => {
    const roleRepository = AppDataSource.getRepository(Role)
    const adminRole = await roleRepository.findOneBy({ id: 1 });
    const userRole = await roleRepository.findOneBy({ id: 2 });
    const usersrepository = AppDataSource.getRepository(User)
    await usersrepository.clear();
    console.log("Inserting a new user into the Memory...")
    var user = new User()
    user.id = 1;
    user.email = "Kim.Doyoung.email.com";
    user.password = "Pass@1234";
    user.gender = "female";
    user.roles = [];
    user.roles.push(adminRole);
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...")
    await usersrepository.save(user);

    user = new User()
    user.id = 2;
    user.email = "Na.Jaemin.email.com";
    user.password = "Pass@1234";
    user.gender = "male";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...")
    await usersrepository.save(user);

    user = new User()
    user.id = 3;
    user.email = "Lee.Haechan.email.com";
    user.password = "Pass@1234";
    user.gender = "male";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...")
    await usersrepository.save(user);

    const user2 = await usersrepository.find({ relations: { roles: true } })
    console.log(JSON.stringify(user2, null, 2));

    const roles = await roleRepository.find({ relations: { users: true } })
    console.log(JSON.stringify(roles, null, 2));

}).catch(error => console.log(error))
